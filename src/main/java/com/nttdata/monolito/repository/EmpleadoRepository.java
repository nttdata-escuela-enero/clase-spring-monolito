package com.nttdata.monolito.repository;

import com.nttdata.monolito.entity.Empleado;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {

  List<Empleado> findByNombreContaining(String text);

  @Query(value = "select new Empleado(p.id,p.nombre) from Empleado p")
  List<Empleado> listarEmpleadosJpa();

  @Query(value = "select * from Empleados", nativeQuery = true)
  List<Empleado> listarEmpleadosSql();
}
