package com.nttdata.monolito.controller;

import com.nttdata.monolito.dto.UsuarioDto;
import com.nttdata.monolito.service.IUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

  @Autowired
  IUsuarioService IUsuarioService;

  @GetMapping("/listar")
  public List<UsuarioDto> listarUsuarios() {
    return IUsuarioService.listarUsuarios();
  }

  @PostMapping("/registrar")
  public UsuarioDto registrarUsuario(@RequestBody UsuarioDto body) {
    return IUsuarioService.registrarUsuario(body);
  }

  @PutMapping("/actualizar/{id}")
  public UsuarioDto actualizar(@RequestBody UsuarioDto body, @PathVariable("id") Long idUsuario) {
    return IUsuarioService.actualizar(body, idUsuario);
  }

  @DeleteMapping("/eliminar/{id}")
  public String eliminar(@PathVariable("id") Long idUsuario) {
    return IUsuarioService.eliminar(idUsuario);
  }
}
