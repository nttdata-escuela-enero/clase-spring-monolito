package com.nttdata.monolito.controller;

import com.nttdata.monolito.dto.RolDto;
import com.nttdata.monolito.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rol")
public class RolController {
    @Autowired
    IRolService IRolService;

    @GetMapping("/listar")
    public List<RolDto> listarRols() {
        return IRolService.listarRoles();
    }

    @PostMapping("/registrar")
    public RolDto registrarRol(@RequestBody RolDto body) {
        return IRolService.registrarRol(body);
    }

    @PutMapping("/actualizar/{id}")
    public RolDto actualizar(@RequestBody RolDto body, @PathVariable("id") Long idRol) {
        return IRolService.actualizar(body, idRol);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable("id") Long idRol) {
        return IRolService.eliminar(idRol);
    }
}
