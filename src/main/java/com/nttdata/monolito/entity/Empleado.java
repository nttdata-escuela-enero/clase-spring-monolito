package com.nttdata.monolito.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Empleados")
public class Empleado {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nombres", length = 120)
  private String nombre;

  @Column(name = "sexo", length = 1, nullable = false)
  private String sexo;

  @Transient
  private String telefono;

  @ManyToOne
  //    @JoinColumn(name = "id_empresa")
  private Empresa empresa;

  @ManyToMany
  @JoinTable(name = "EmpleadoRoles", joinColumns = @JoinColumn(name = "empleado_id"),
      inverseJoinColumns = @JoinColumn(name = "rol_id"))
  //    @JsonIgnoreProperties("empleados")
  private List<Rol> roles;

  //  @OneToOne(mappedBy = "empleado")
  //  Usuario usuario;

  public Empleado(Long id, String nombre) {
    this.id = id;
    this.nombre = nombre;
  }
}
