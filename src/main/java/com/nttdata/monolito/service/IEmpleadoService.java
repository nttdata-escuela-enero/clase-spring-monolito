package com.nttdata.monolito.service;

import com.nttdata.monolito.dto.EmpleadoDto;
import com.nttdata.monolito.entity.Empleado;
import java.util.List;

public interface IEmpleadoService {

  List<EmpleadoDto> listarEmpleados();

  List<EmpleadoDto> filtrarEmpleadosPorNombre(String texto);

  EmpleadoDto registrarEmpleado(EmpleadoDto body);

  EmpleadoDto actualizar(EmpleadoDto body, Long idEmpleado);

  String eliminar(Long idEmpleado);
}
