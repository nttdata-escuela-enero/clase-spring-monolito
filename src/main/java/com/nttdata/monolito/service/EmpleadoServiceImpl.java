package com.nttdata.monolito.service;

import com.nttdata.monolito.dto.EmpleadoDto;
import com.nttdata.monolito.entity.Empleado;
import com.nttdata.monolito.entity.Empresa;
import com.nttdata.monolito.entity.Rol;
import com.nttdata.monolito.repository.EmpleadoRepository;
import com.nttdata.monolito.repository.EmpresaRepository;
import com.nttdata.monolito.repository.RolRepository;
import com.nttdata.monolito.utils.Utils;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService {

  @Autowired
  EmpleadoRepository empleadoRepository;

  @Autowired
  EmpresaRepository empresaRepository;

  @Autowired
  RolRepository rolRepository;

  @Override
  public List<EmpleadoDto> listarEmpleados() {
    return empleadoRepository.findAll().stream().map(empleado -> {
      return Utils.toEmpleadoDto(empleado,
                    Utils.toEmpresaDto(empleado.getEmpresa()),
                    Optional.ofNullable(empleado.getRoles()).map(roles -> {
                      return roles.stream().map(Utils::toRolDto).collect(Collectors.toList());
                    }).orElse(null)
              );
    }).collect(Collectors.toList());
  }

  @Override
  public List<EmpleadoDto> filtrarEmpleadosPorNombre(String texto) {
    return empleadoRepository.findByNombreContaining(texto).stream()
        .map(empleado -> Utils.toEmpleadoDto(empleado, Utils.toEmpresaDto(empleado.getEmpresa()), Optional.ofNullable(empleado.getRoles())
            .map(roles -> roles.stream().map(Utils::toRolDto).collect(Collectors.toList())).orElse(null)))
        .collect(Collectors.toList());
  }

  @Override
  public EmpleadoDto registrarEmpleado(EmpleadoDto body) {
    Optional<Empresa> empresa = empresaRepository.findById(body.getEmpresaId());
    if (empresa.isPresent()) {
      Empleado empleadoRegister = new Empleado(null, body.getNombre(), body.getSexo(), body.getTelefono(), empresa.get(), null);
      Empleado empleadoRegistered = empleadoRepository.save(empleadoRegister);
      return Utils.toEmpleadoDto(empleadoRegistered, Utils.toEmpresaDto(empleadoRegistered.getEmpresa()),
          Optional.ofNullable(empleadoRegistered.getRoles())
              .map(roles -> roles.stream().map(Utils::toRolDto).collect(Collectors.toList())).orElse(null));
    }
    return null;
  }

  @Override
  public EmpleadoDto actualizar(EmpleadoDto body, Long idEmpleado) {
    Optional<Empleado> encontrado = empleadoRepository.findById(idEmpleado);
    Optional<Empresa> empresa = empresaRepository.findById(body.getEmpresaId());
    List<Rol> rolList = rolRepository.findAllById(body.getRolesId());
    if (encontrado.isPresent() && empresa.isPresent() && rolList.size() == body.getRolesId().size()) {
      Empleado empleadoUpdate = encontrado.get();
      empleadoUpdate.setId(idEmpleado);
      empleadoUpdate.setNombre(body.getNombre());
      empleadoUpdate.setSexo(body.getSexo());
      empleadoUpdate.setTelefono(body.getTelefono());
      empleadoUpdate.setEmpresa(empresa.get());
      empleadoUpdate.setRoles(rolList);
      Empleado empleadoUpdated = empleadoRepository.save(empleadoUpdate);
      return Utils.toEmpleadoDto(empleadoUpdated, Utils.toEmpresaDto(empleadoUpdated.getEmpresa()),
          Optional.ofNullable(empleadoUpdated.getRoles())
              .map(roles -> roles.stream().map(Utils::toRolDto).collect(Collectors.toList())).orElse(null));
    }
    return null;
  }

  @Override
  public String eliminar(Long idEmpleado) {
    Optional<Empleado> encontrado = empleadoRepository.findById(idEmpleado);
    if (encontrado.isPresent()) {
      empleadoRepository.deleteById(idEmpleado);
      return "Empleado eliminado correctamente";
    }
    return "Empleado no se encuentra registrado";
  }
}
