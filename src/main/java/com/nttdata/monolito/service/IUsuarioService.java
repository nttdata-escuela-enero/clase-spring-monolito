package com.nttdata.monolito.service;

import com.nttdata.monolito.dto.UsuarioDto;
import com.nttdata.monolito.entity.Usuario;
import java.util.List;

public interface IUsuarioService {

  List<UsuarioDto> listarUsuarios();

  UsuarioDto registrarUsuario(UsuarioDto body);

  UsuarioDto actualizar(UsuarioDto body, Long idUsuario);

  String eliminar(Long idUsuario);
}
